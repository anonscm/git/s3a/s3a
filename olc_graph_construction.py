# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 12:02:34 2018

@author: david
"""

from Bio import SeqIO
import networkx as nx
import collections
from difflib import SequenceMatcher


class Read:
    """ Read object containing all information relative to a single annotated read
    Attributes:
        queryId(str) : read identifier
        seq(str) : read amino acid sequence
        nseq(str) : read nucleotidic sequence
        modelStart(int) : starting position of read's domain hit
        modelEnd(int) : ending position of read's domain hit
        pfamId(str) : annotated domain name
    """
    
    def __init__(self, queryId):
        self.queryId = queryId
        self.seq = ''
        self.nseq = ''
        self.start = 0
        self.end = 0
        self.modelStart = 0
        self.modelEnd = 0
        self.modelId = ''
        self.pfamId = ''
        self.length = 0
        self.bitscore = 0.
        self.eValue = 0.
        
    def __repr__(self):
        return repr((self.queryId, self.length, self.start, self.end, self.modelStart, self.modelEnd, self.modelId, self.pfamId,
                     self.bitscore, self.eValue))
   
    def set_id(self, queryId):
        self.queryId = queryId
        
    def set_seq(self, seq):
        self.seq = seq
        self.length = len(seq)
    
    def set_nseq(self, nseq):
        self.nseq = nseq
    
    def set_start(self, seqStart):
        self.start = seqStart
    
    def set_end(self, seqEnd):
        self.end = seqEnd
    
    def set_model_start(self, modelStart):
        self.modelStart = modelStart
    
    def set_model_end(self, modelEnd):
        self.modelEnd = modelEnd
    
    def set_model_id(self, modelId):
        self.modelId = modelId
    
    def set_pfam_Id(self, pfamId):
        self.pfamId = pfamId
        
    def set_bitscore(self, bitscore):
        self.bitscore = bitscore
            
    def set_Evalue(self, eValue):
        self.eValue = eValue


def parse_annotation(prediction_file, fasta_file, nucl_seq_file):
    """ Parse nucleotidic, amino acid and hmm files to create Read objects
    Args:
        prediction_file : annotation file
        fasta_file : amino acid file
        nucl_seq_file : nucleotidic file
    
    Returns : 
        G : Graph contains a node for each Read object
        domain_centered_dict : dictionnary containing a list of Reads(values) for each domain(key)
    """
    
    # create query dictionnary
    query_dict = SeqIO.index(fasta_file, "fasta")
    # create nucleotidic sequence dictionnary
    nucl_dict = SeqIO.index(nucl_seq_file, "fasta")
    # Create domain centered dictionnary
    domain_centered_dict = collections.defaultdict(list)
    # Create graph
    G = nx.DiGraph()
    
    with open(prediction_file) as f:
        # remove first element
        next(f)
        for line in f:
            elements = line.split()

            pfamId = elements[5]
            queryId = elements[6]
            # set read class values
            read = Read(queryId)
            read.set_seq(query_dict[queryId].seq)
            '''
            # +,- <=> 1,2 in faa, so need to convert nucl seq id to find corresponding aa seq id
            lastCar = queryId[-1]
            if lastCar == "1" :
                newCar = "+"
            else:
                newCar = "-"
            nkey = queryId[:-1] + newCar
            read.set_nseq(nucl_dict[nkey].seq)
            #'''
            read.set_nseq(nucl_dict[queryId].seq)
            read.set_start(int(elements[8]))
            read.set_end(int(elements[9]))
            read.set_model_start(int(elements[3]))
            read.set_model_end(int(elements[4]))
            read.set_model_id(elements[2])
            read.set_pfam_Id(elements[5])
            read.set_bitscore(float(elements[1]))
            read.set_Evalue(float(elements[0]))
            # Add to domain centered dictionnary            
            domain_centered_dict[pfamId].append([int(elements[8]),int(elements[9]),read])
            # Add node in graph
            G.add_node(read.queryId, seq = read.seq)
    return G, domain_centered_dict

def get_pos_overlap_length(begin1, end1, begin2, end2):
    """ Simple function to get a domain overlap between two reads from their domain hit starting and ending position
    Args : 
        begin1(int),end1(int): starting and ending domain hit position for first read
        begin2(int),end2(int): starting and ending domain hit position for second read
    Returns:
        overlap length, 0 if no overlap
    """
    return max( min(end1, end2) - max(begin1, begin2) + 1, 0)

def order_list(reads_list):
    """Orders a list of reads from their domain hit starting/ending position
    Args :
        reads_list (list): list of reads with starting and ending hit position
    Returns:
        sorted list of reads
    """
    
    sorted_list = []
    for start,end,read in reads_list:
        sorted_list.append([start,1,read])
        sorted_list.append([end,-1,read])
        
    return sorted(sorted_list)    


'''
def overlap_reads(readpoints):
    sorted_list = []
    for start,end,read in readpoints:
        sorted_list.append([start,1,read])
        sorted_list.append([end,-1,read])
    sorted_list = sorted(sorted_list)
    
    count = 0
    current_reads = []
    for position,point,read in sorted_list:
        if point == 1:
            count += 1
            if count > 1:
                for r in current_reads:
                    print(read,r)
            current_reads.append(read)
        else:
            count -= 1
            current_reads.remove(read)
    
    return sorted_list
'''




def create_OLC_Graph(prediction_file, fasta_file, nucl_seq_file, domainOverlapThreshold = 20., identityThreshold = 0.6):
    """ Creation of the OLC Graph
    Args : 
        prediction_file : annotation file
        fasta_file : amino acid file
        nucl_seq_file : nucleotidic file
        (optional) domainOverlapThreshold : threshold for the domain overlap
        (optional) identityThreshold : threshold for the identity percentage
    Returns:
        G : a networkx OLC graph
            Each node corresponds to a Read
            Each edge has the following attributes:
                read.queryId(str) :  starting Read name
                read2.queryId(str) : ending Read name
                domain(str) :  domain name
                overlap(int) : domain overlap value
                identity(float) : identity percentage (ip)
                longestMatch(int) : longest matching substring (lms) value
    """
    # node creation, domain annotation /fasta files parsing    
    G, domain_dictionnary = parse_annotation(prediction_file, fasta_file, nucl_seq_file)
    # Edge creation for each domain:
    for domain in domain_dictionnary.keys():
        onedomain = domain_dictionnary[domain]
        # order list by start/end position from domain annotation
        # read > start and read > end created for each read
        sortedonedomain = order_list(onedomain)
        count = 0
        current_reads = []
        # Parse by increasing position. 
        # Each time a new starting position comes by, new interval for each read in queue
        for position,point,read in sortedonedomain:
            if point == 1:
                count += 1
                if count > 1:
                    for read2 in current_reads:
                        domain_overlap = get_pos_overlap_length(read.modelStart,read.modelEnd,read2.modelStart,read2.modelEnd)
                        # if domain overlap is shorter than the given threshold, dont consider the pair
                        if (domain_overlap >= domainOverlapThreshold):
                            # Compute identity percentage for nucleotides
                            readnQuery1 = read.nseq[read.start*3:read.end*3]
                            readnQuery2 = read2.nseq[read2.start*3:read2.end*3]
                            matcherNucleo = SequenceMatcher(None, readnQuery1,readnQuery2)                            
                            identityPercentageNucleo = matcherNucleo.ratio()
                            # Compute lms
                            longestMatch = matcherNucleo.find_longest_match(0,len(readnQuery1)-1,0,len(readnQuery2)-1)[2]
                            # if identity is two low, pass
                            if identityPercentageNucleo < identityThreshold:
                                break
                            # If not, add edge with all information, in the right order
                            if read.modelStart < read2.modelStart :
                                # Remmber that the graph is directed
                                G.add_edge(read.queryId, read2.queryId, domain = domain, overlap = domain_overlap, identity = identityPercentageNucleo, longestMatch = longestMatch)
                            else:
                                G.add_edge(read2.queryId, read.queryId, domain = domain, overlap = domain_overlap, identity = identityPercentageNucleo, longestMatch = longestMatch)
                current_reads.append(read)
            else:
                count -= 1
                current_reads.remove(read)
    return G



# S3A Assembler

S3A is a fast and accurate gene-targeted assembler based on the Overlap-Layout Consensus paradigm to reconstruct coding regions from domain annotated metagenomic sequence reads. 


<img src="schema.png"
     width="450" height="450"
     style="float: right; margin-right: 10px;" />

## Getting Started

To start S3A, run the shell script `S3A_Assembler.py` with the following arguments: 

-p : annotation file

-n : fasta file for the read nucleotidic sequences

-a : fasta file for the read amino acid sequences

-o : domain overlap threshold (default 20)

-l : longest matching substring threshold (default 0.18)

-i : identity percentage threshold (default 0.6)

Example:

    python S3A_Assembler.py 
            -p annotatedReads
            -n arch_bact_454Ti_7X.pCDS.ffn
            -a arch_bact_454Ti_7X.pCDS.faa

## Prerequisites

S3A runs with Python 2.7, and the following libraries are necessary:

- __[NetworkX](https://networkx.github.io/)__ (1.11)
- __[BioPython](https://biopython.org/)__(1.68)

S3A needs as input:

- the domain annotation file.
- the read sequences fasta file.

S3A generates a text file containing the list of reads assembled.


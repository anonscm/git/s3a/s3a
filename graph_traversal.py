# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 14:54:11 2017

@author: david
"""

import networkx as nx
import Queue            

class Contig:
    """ Contig class definition.
    Attributes : 
        nodes ()
    """
    def __init__(self, name):
        self.name = name
        nodes = []
        #members = set()
        path = set()
        seq = ''
        length = 0
        overlap = []
    
    def __repr__(self):
        return repr((self.name, self.nodes, 
                     self.seq, self.length))

    def set_nodes(self, nodes):
        self.nodes = nodes 
 
    '''
    def set_members(self, members):
        self.members = members 
    '''

    def set_seq(self, seq):
        self.seq = seq
        self.length = len(seq)


    def set_path(self, path):
        self.path = path
        
        
    def set_name(self, name):
        self.name = name
        
    def set_overlap(self,overlap):
        self.overlap = overlap
        

def simple_trimmer(G,criteria,threshold):
    """ removes edges that are below given threshold for the given criteria
    Args
        criteria : 'identity' or longestMatch'
    """
    edgesToRemove = []
    for node1, node2 in G.edges_iter():
        crit = G[node1][node2][criteria]
        if crit < threshold:
            #print longestmatch
            edgesToRemove.append([node1,node2])
    for couple in edgesToRemove:
        G.remove_edge(couple[0], couple[1])    
    return

def transitive_reduction(G,scoreType,estimator = 'longestMatch'):
    """ Performs a dfs transitivity reduction
    Args
        G : graph
        scoreType : 'min' or 'mean' depending on whether the score should be the least along the path, or the mean
        estimator : edge value used to estimate the quality of the transitivity edge
    Returns : 
        transitivity dictionnary
    """
    transitive_dictionnary = {}
    bad_transitive_edges = []
    edges_to_remove = []
    loops = 0
    # For every node u in the graph
    for u in nx.nodes_iter(G):
        # For node v such that (u,v) is an edge
        for v in G.successors(u):
            #dfs as a list of edges
            dfs_edges = nx.dfs_edges(G,v)
            score = update_score(G,(u,v),(0.,0),scoreType,estimator = 'longestMatch')
            for edge in dfs_edges:
                #update score for dfs path
                score = update_score(G,edge,score,scoreType,estimator = 'longestMatch')
                w = edge[1]
                if G.has_edge(u,w):
                    loops += 1
                    # this edge is transitive, check score value to estimate its worth
                    transScore = G[u][w][estimator]
                    if transScore >= score[0] :
                        transitive_dictionnary[(u, w)] = True
                    else : 
                       bad_transitive_edges.append([u,w]) 

                    edges_to_remove.append( (u,w) )

    G.remove_edges_from(edges_to_remove)
    return transitive_dictionnary


def update_score(G,edge,score,scoreType,estimator = 'longestMatch'):
    """ Update score for the given edge
    Args : 
        G : graph
        edge : edge to be updated
        scoreType : 'min' or 'mean' depending on whether the score should be the least along the path, or the mean
        estimator : edge value to be used for the score
    Returns the updated score
    """
    edgeScore = G[ edge[0] ][ edge[1] ][estimator]
    if scoreType == 'min':
        if score[0] == 0. : 
            score = (edgeScore,1)
        else:
            if edgeScore <= score[0]:
                score = (edgeScore,1)
    elif scoreType == 'mean' :
        if score[0] == 0.:
            score = (edgeScore,1)
        else:
            nedges = score[1]
            score = ((score[0]*nedges + edgeScore) / (nedges + 1), (nedges + 1) ) 
    else:
    #sum
        score[0] += edgeScore
    return score


def combine_single_paths(G):
    """ Regroup any couple of nodes were node1_out_degree ==1 and node2_in_degree ==1.
    Replaceby a single node. Update edges to the union of nodes.
    """
    while True:
        nodes_to_combine = []
        # combine backward the nodes with in-degree = 1
        for node in G.nodes_iter():
            in_deg = G.in_degree(node)
            if in_deg == 1:
                pred = G.predecessors(node)[0]
                out_deg_pred = G.out_degree(pred)
                if out_deg_pred == 1:
                    nodes_to_combine.append(node)
        # continue until there is no qualified nodes
        if len(nodes_to_combine) == 0:
            break
        for curr in nodes_to_combine:
            # predecessor of current node
            pred = G.predecessors(curr)[0]
            # incoming edges to the combined node
            predspreds = G.predecessors(pred)
            # outgoing edges from the combined node
            succs = G.successors(curr)
            # create a new node
            new_node = combine_two_nodes(pred, curr, G)
            # make links between the new node and other nodes
            for p in predspreds:
                old_edge_data = G[p][pred]
                G.add_edge(p, new_node, old_edge_data)
            for s in succs:
                old_edge_data = G[curr][s]
                G.add_edge(new_node, s, old_edge_data)
            # remove used nodes
            G.remove_node(curr)
            G.remove_node(pred)
            if curr in nodes_to_combine:
                nodes_to_combine.remove(curr)
            if pred in nodes_to_combine:
                nodes_to_combine.remove(pred)

def tag_crosses(G):
    """ retrieves all chimeric nodes
    """
    central_nodes = []
    for node in G.nodes_iter():
        if G.out_degree(node) > 1 and G.in_degree(node) > 1 :
            #predecessors = G.predecessors(node)
            #successors = G.successors(node)
            #matches = []
            #for pred in predecessors:
            #    for succ in successors:
            #        if G.has_edge(pred,succ)
            central_nodes.append(node)
    return central_nodes


def remove_crosses(G,central_nodes):
    """ Remove chimeric nodesif needs be
    """
    for node in central_nodes:
        G.remove_node(node)
    return

def find_simple_links(G):
    """ Finds nodes with only one edge between them
    """
    simple_list = []
    for node1, node2 in G.edges_iter():
        if G.out_degree(node1) == 1 and G.in_degree(node2) == 1:
            simple_list.append([node1,node2])
    return simple_list

def merge2nodes(G,node1,node2):
    """ Combine two nodes in one.
    Update the sequence and the name with node1Name|node2Name
    """
    # Retrieve node sequence
    seq1 = str(G.node[node1]['seq'])
    seq2 = str(G.node[node2]['seq'])
    
    # create new node merging the 2 nodes
    newnodename = node1 + "|" + node2
    #overlap = get_seq_overlap_length(seq1,seq2)[0]
    #newnodeseq = seq1 + seq2[overlap:]
    overlap = G.edge[node1][node2]['overlap']
    newnodeseq = seq1 + seq2[overlap:]
    # add it, removal is afterwards
    G.add_node(newnodename, seq = newnodeseq)
    
    # update edges for node 1
    for pred in G.predecessors(node1):
        # Gather info
        current_edge_data = G.edge[pred][node1]
        #remove edge and put the new one
        G.add_edge(pred, newnodename, current_edge_data)
        
    #Same for node 2
    for succ in G.successors(node2):
        # Gather info
        current_edge_data = G.edge[node2][succ]
        #remove edge and put the new one
        G.add_edge(newnodename,succ, current_edge_data)
    return

def simplify_graph(G):
    """ Reduce path from the graph by merging nodes that only have one edge between them
    """
    simpleEdges = find_simple_links(G)
    for couple in simpleEdges:
        merge2nodes(G,couple[0],couple[1])
    # Remove the now useless nodes (edges should automatically disappear)
    flatlist = [item for sublist in simpleEdges for item in sublist]
    flat = list(set(flatlist))
    for n in flat:
        G.remove_node(n)
    return

def extract_multinodes_from_contig(contigs):
    """ Processed to extract single reads when they have been combined
    """
    contigs_list = []
    for contig in contigs:
        contig_list = []
        pat = contig.path
        for el in pat:
            spl = el.split("|")
            for e in spl:
                contig_list.append(e)
        contigs_list.append(contig_list)
    return contigs_list

def write_contigs(contigslist):
    contigfile = open('contiglist', 'a+')
    for contig in contigslist:
        contigfile.write("1: ")
        for i in range(len(contig)-1):
            contigfile.write(contig[i+1])
            contigfile.write(" ")
        contigfile.write("\n")
    return
       
# add root to transfer node score to edge weight.
def add_root_to_subgraph(subgraph):
    """ add a new node called root to every subgraph, from which the traversal will be initiated
    """
    assert 'root' not in subgraph.nodes()
    subgraph.add_node('root', seq = '')
    for node in subgraph.nodes():
        if node != 'root' and subgraph.in_degree(node) == 0:
            subgraph.add_edge('root', node, domain = '', overlap = 0, identity = 0., longestMatch = 0)



def get_seq_overlap_length(seq1, seq2):
    """ Gets the overlap length of two reads. return 0 if no overlap.
    """
    # no string should contain the other.
    hamming_thres = 3 
    len1 = len(seq1)
    len2 = len(seq2)
    max_overlap = 0 # currently maximum overlap.
    hamming_distance = 0
    # loop over number of overlappd pos.
    for i in xrange(min(len1, len2), 0, -1):
        hamming_distance = get_hamming_distance(seq1[-i:], seq2[:i])
        if hamming_distance <= hamming_thres:
            max_overlap = i
            break
    return (max_overlap, hamming_distance)   

def get_hamming_distance(seq1, seq2):
    """ Computes the Hamming distance between two sequences
    """
    assert len(seq1) == len(seq2)
    hamming_distance = 0
    for i in range(len(seq1)):
        if seq1[i] != seq2[i]:
            hamming_distance += 1
    return hamming_distance


def get_true_overlap(seq1,seq2):
    trueOverlap = get_seq_overlap_length(seq1,seq2)
    return (seq1[:-trueOverlap[0]] + seq2)
    

def get_true_overlap_from_edge(node1,node2,G):
    """ Given two nodes, computes the overlap given each node sequence
    """
    seq1 = G.node[node1]['seq']
    seq2 = G.node[node2]['seq']
    trueOverlap = get_seq_overlap_length(seq1,seq2)
    return (seq1[:-trueOverlap] + seq2)

# get the contig from a path.
def get_contig_from_path(path, name, G):
    """ Create the Contig object from a given path
    Args:
    path : list of nodes
    name : contig name
    G : graph
    
    Returns:
        Contig object
    """
    assert path[0] == 'root'
    contig = Contig(name)
    # path[0] is always root.
    contig_seq = ''
    contigScore = 0.
    nodes = []
    contigOverlaps = []
    #members = set()
    contig_path = path[:]
    for i in xrange(1, len(path)):
        begin_node = path[i-1]
        end_node = path[i]
        nodes.append(end_node)
        #members |= G.node[end_node]['members']
        # concatenate sequence.
        #overlap = G[begin_node][end_node]['overlap']
        begin_seq = G.node[begin_node]['seq']
        end_seq = G.node[end_node]['seq']
        overlap = get_seq_overlap_length(begin_seq,end_seq)[0]
        contig_seq += end_seq[overlap:]
        contigOverlaps.append(overlap)
        #contig_seq = get_true_overlap(contig_seq,current_node)
        #print overlap, len(seq), seq
    #print len(contig), contig
    contig.set_score( contigScore/len(path) )
    contig.set_seq(contig_seq)
    contig.set_nodes(nodes)
    #contig.set_members(members)
    contig.set_path(contig_path)
    contig.set_overlap(contigOverlaps)
    return contig



def get_contigs_from_paths(paths, G):
    """ Creates contigs from the given paths
    """
    contigs = []
    i = 1
    for path in paths:
        name = i
        contig = get_contig_from_path(path, name, G)
        #contig.set_id()
        contigs.append(contig)
        i += 1
    return contigs


def get_seq_from_path(path, G):
    """ Gets the assembled sequence from a given path
    """
    assert path
    node = path[0]
    path_seq = G.node[node]['seq']
    for i in range(1, len(path)):
        node1 = path[i-1]
        node2 = path[i]
        seq1 = G.node[node1]['seq']
        seq2 = G.node[node2]['seq']
        overlap = get_seq_overlap_length(seq1,seq2)[0]
        path_seq += seq2[overlap:]
    return path_seq

def get_contigs_from_graph(G,supports):
    add_root_to_subgraph(G)
    rectangles = get_rectangles('root',G)
    target_nodes = get_target_nodes(rectangles, G)
    target_node_set = get_target_node_set(target_nodes)
    path = []
    paths = []
    curr_node = 'root'
    dfs(G, curr_node, path, paths, target_nodes, target_node_set, supports)
    return paths
    contigs = get_contigs_from_paths(paths, G)
    return contigs


def are_all_successors_single(G, node):
    """ See if all successors from given node are sink nodes
    """
    successors = G.successors(node)
    for succ in successors:
        if G.out_degree(succ) != 0:
            return False
    return True

def are_all_predecessors_single(G, node):
    """ See if all predecessors from given node have no predecessors
    """
    predecessors = G.predecessors(node)
    for pred in predecessors:
        if G.in_degree(pred) != 0:
            return False
    return True


def retrieve_tips(G):
    """ Gets nodes that have more than one successor, and all of them have no successor
    """
    tips = []
    for node in G.nodes_iter():
        if len(G.out_edges(node)) > 1 and are_all_successors_single(G,node) == True:
            tips.append(node)
    return tips

def retrieve_in_tips(G):
    """ Retrieve nodes that have multiple predecessors, none of which have predecessors
    """
    tips = []
    for node in G.nodes_iter():
        if len(G.in_edges(node)) > 1 and are_all_predecessors_single(G,node) == True:
            tips.append(node)
    return tips

# criteria can be , identity percentage, etc..
def remove_in_tips(G,criteria):
    """ removes in_tips
    """
    intips = retrieve_in_tips(G)
    while(intips):
        for node in intips:
            all_preds = G.predecessors(node)
            bestCriteria = -1000
            for tip in all_preds:
                cri = G[tip][node][criteria]
                if cri > bestCriteria:
                    bestCriteria = cri
                    bestNode = tip
            for badNode in all_preds:
                if badNode != bestNode:
                    G.remove_edge(badNode,node)         
        intips = retrieve_in_tips(G)


def remove_tips(G,criteria):
    """ Gets all tips. Keep only edge with the highest criteria value
    Criteria can be 'identity', 'overlap', 'longestMatch'
    """
    tips = retrieve_tips(G)
    while(tips):
        for node in tips:
            all_succs = G.successors(node)
            bestCriteria = -1000
            for tip in all_succs:
                cri = G[node][tip][criteria]
                if cri > bestCriteria:
                    bestCriteria = cri
                    bestNode = tip
            for badNode in all_succs:
                if badNode != bestNode:
                    G.remove_edge(node, badNode)         
        tips = retrieve_tips(G)
    return
            
def get_sink_nodes(G):
    """ gets the nodes with no successor
    """
    sink_nodes = []
    for node in G.nodes_iter():
        if G.out_degree(node) == 0:
            sink_nodes.append(node)
    return sink_nodes

def remove_cycles(G):
    """ removes the cycles to make the graph directed acyclic
    removes the edge with the lowest overlap value
    """
    while not nx.is_directed_acyclic_graph(G):
        subgraphs = nx.strongly_connected_component_subgraphs(G)
        for subgraph in subgraphs:
            if subgraph.number_of_nodes() > 1:
                min_edge = get_shortest_edge(subgraph)
                #print subgraph.edges()
                #print min_edge
                G.remove_edge(*min_edge)

def get_shortest_edge(G):
    """ returns the edge with the least domain overlap
    """
    min_overlap = 100000
    for node1, node2 in G.edges_iter():
        if G[node1][node2]['overlap'] < min_overlap:
            min_edge = (node1, node2)
            min_overlap = G[node1][node2]['overlap']
    return min_edge



def get_rectangles(source_node, G):
    """ gets the rectangles from a given node
    """
    # the ending node and a pred of an alternative path.
    # the first element is the revisited node and the second one is its pred.
    joints = []
    q = Queue.Queue()
    visited = {}
    q.put(source_node)
    visited[source_node] = True
    aa = 0
    while not q.empty():
        aa +=1
        if aa%100 == 0:
            print aa
        curr = q.get()
        for succ in G.successors(curr):
            if succ in visited:
                joints.append((succ, curr))
            else:
                visited[succ] = True
                q.put(succ)
    # the begin and end nodes of rectangles.
    rectangles = set()
    for curr, pred in joints:
        for other_pred in G.predecessors_iter(curr):
            if other_pred != pred:
                lca = get_lca(other_pred, pred, G)
                if lca:
                    rectangles.add((lca, curr))
    return rectangles


def get_all_preds(node_set, G):
    """ gets all predecessors from set of nodes in the graph
    """
    preds = set()
    for node in node_set:
        preds |= set(G.predecessors(node))
    return preds

def get_lca(node1, node2, G):
    """ gets the least common ancestor between two nodes
    does not include these two nodes
    """
    lca = None
    all_preds1 = set()
    all_preds2 = set()
    all_preds1.add(node1)
    all_preds2.add(node2)
    preds1 = set(G.predecessors(node1))
    preds2 = set(G.predecessors(node2))
    while preds1 or preds2:
        all_preds1 |= preds1
        all_preds2 |= preds2
        ca_set = all_preds1 & all_preds2
        if ca_set:
            sorted_nodes_list = nx.topological_sort(G)
            ca_list = list(ca_set)
            lca = max(ca_list, key=lambda ca: sorted_nodes_list.index(ca))
            break
        else:
            preds1 = get_all_preds(preds1, G)
            preds2 = get_all_preds(preds2, G)
    return lca

def get_target_node_set(target_nodes):
    """ Put list of nodes in a set
    """
    target_node_set = set()
    for nodes in target_nodes:
        for node in nodes:
            target_node_set.add(node)
    return target_node_set

def has_support(curr_node, node, supports):
    """ Check whether two nodes have an edge in the transitivity dictionnary
    """
    return (curr_node, node) in supports or \
           (node, curr_node) in supports

def get_target_nodes(rectangles, G):
    target_nodes = []
    for begin, end in rectangles:
        for connected_path in nx.all_simple_paths(G, begin, end):
            target_nodes.append(tuple(connected_path[1:-1]))
    sinks = get_sink_nodes(G)
    for sink in sinks:
        for pred in G.predecessors_iter(sink):
          if G.out_degree(pred) > 1 and G.in_degree(pred) > 1:
              target_nodes.append((sink,))
    return target_nodes


def is_valid_path_node(curr_node, path, target_nodes, target_node_set, supports):
    """ Check whether the node has support
    """
    if curr_node not in target_node_set:
        return True
    elif not set(path) & target_node_set:
        return True
    else:
        # if any of its previous nodes in the same rectangle is added.
        for nodes in target_nodes:
            if curr_node in nodes and nodes[0] != curr_node:
                return True
        for nodes in target_nodes:
            if curr_node not in nodes:
                for node in nodes:
                    if node in path and has_support(curr_node, node, supports):
                        return True
    return False

def dfs(G, curr_node, path, paths,target_nodes, target_node_set, supports):
    """ Simple depth first search
    """
    if not is_valid_path_node(curr_node, path, target_nodes,
                              target_node_set, supports):
        return

    path.append(curr_node)
    if G.out_degree(curr_node) == 0:
        paths.append(path[:])
    else:
        for succ in G.successors_iter(curr_node):
            dfs(G, succ, path, paths,
                target_nodes, target_node_set, supports)
    path.pop()

def graph_traversal(G, lmsThreshold = 0.18, ipThreshold = 0.8):
    """Whole traversal procedure
    Output : contigs in text file
    """
    # First step is trimming the graph for the two threholds
    simple_trimmer(G,'identity',ipThreshold)
    simple_trimmer(G,'longestMatch',lmsThreshold)
    # Second step is to make the graph directed acyclic
    G.remove_nodes_from(nx.isolates(G))
    remove_cycles(G)
    # Simplify the graph by combining reads in simple paths
    simplify_graph(G)
    # Perform the transitive reduction to recover transitivity edges
    supports = transitive_reduction(G,'min')
    # Remove crosses
    chimeric_nodes = tag_crosses(G)
    remove_crosses(G,chimeric_nodes)
    remove_tips(G,"longestMatch")
    remove_in_tips(G, "longestMatch")
    # Traverse
    contigs = get_contigs_from_graph(G,supports)
    # Separate reads that had been combined
    #simple_contigs = extract_multinodes_from_contig(contigs)
    #write_contigs(contigs)
    return contigs


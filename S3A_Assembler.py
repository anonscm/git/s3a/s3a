# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 18:46:17 2018

@author: david
"""
import olc_graph_construction as olc
import graph_traversal as tr
import argparse

def main():
    '''
    print sys.argv
    annotatedReads,nfile,afile,do,lms,ip = sys.argv[1:]
    print "Prediction file: ", annotatedReads
    print "nt fasata file: ", nfile
    print "aa fasta file: ",afile
    print "domain overlap, lms, ip: " ,do,lms,ip
    '''
    '''
    annotatedReads = "annotatedReads"
    nfile = "arch_bact_454Ti_7X.pCDS.faa"
    afile = "arch_bact_454Ti_7X.pCDS.ffn"
    do,lms,ip = 20,80,0.8
    '''
    '''
    G = olc.create_OLC_Graph(annotatedReads, afile, nfile, do, 0.6)
    conts = tr.graph_traversal(G,lms)
    '''
    parser = argparse.ArgumentParser("S3A Assembler")
    parser.add_argument(
        "-p",
        dest="annotatedReads",
        required=True,
        help="Annotation fasta file.")
    parser.add_argument(
        '-n',
        dest='nfile',
        required=True,
        help='Nucleotidic fasta file for sequenced reads.')
    parser.add_argument(
        "-a",
        dest="afile",
        required=True,
        help="Amino acide fasta file for sequenced reads.")
    parser.add_argument(
        '-o',
        dest='do',
        default=20,
        type=int,
        help="Domain overlap threshold.")
    parser.add_argument(
        "-l",
        dest="lms",
        default=80,
        type=int,
        help="Longest matching substring threshold.")
    parser.add_argument(
        '-i',
        dest='ip',
        default=0.8,
        type=float,
        help='Identity percentage threshold.')
    parser.add_argument(
        '-v', '--verbose', action='store_true', help='Increase output verbosity')

    args = parser.parse_args()

    G = olc.create_OLC_Graph(args.annotatedReads, args.afile, args.nfile, args.do, 0.6)
    conts = tr.graph_traversal(G,lms)
    
    
if __name__ == '__main__':
    main()
